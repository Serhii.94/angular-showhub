import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotYetAvailableComponent } from './not-yet-available.component';

describe('NotYetAvailableComponent', () => {
  let component: NotYetAvailableComponent;
  let fixture: ComponentFixture<NotYetAvailableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotYetAvailableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NotYetAvailableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
