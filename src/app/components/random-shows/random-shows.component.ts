import { Component, EventEmitter, Input, Output } from '@angular/core';

import { UI_CONSTANTS } from 'src/app/constants';

@Component({
  selector: 'app-random-shows',
  templateUrl: './random-shows.component.html',
  styleUrls: ['./random-shows.component.scss'],
})
export class RandomShowsComponent {
  UI = UI_CONSTANTS;

  @Input() isDisabled = false;
  @Output() randomClicked = new EventEmitter<void>();

  handleRandomClick() {
    this.randomClicked.emit();
  }
}
