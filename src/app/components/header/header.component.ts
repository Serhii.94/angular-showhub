import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import firebase from 'firebase/compat';

import { NavigationService, AuthService, ModalService } from 'src/app/services';

import { UI_CONSTANTS, ROUTES } from 'src/app/constants';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  UI = UI_CONSTANTS;
  routes = ROUTES;
  isUserLogged: Observable<firebase.User | null> = this.authService.getUser();

  constructor(
    public navigationService: NavigationService,
    public modalService: ModalService,
    private authService: AuthService,
  ) {}

  logOut() {
    this.authService.logOut();
  }

  googleLogin() {
    this.authService.googleLogIn();
    this.modalService.close();
  }
}
