import { Component, Input } from '@angular/core';

import { IActor } from 'src/app/models';

import { MAIN_PATH, ROUTES, UI_CONSTANTS } from 'src/app/constants';

@Component({
  selector: 'app-actor',
  templateUrl: './actor.component.html',
  styleUrls: ['./actor.component.scss'],
})
export class ActorComponent {
  @Input() actor: IActor;
  UI = UI_CONSTANTS;
  routes = ROUTES;
  main = MAIN_PATH;

  addToFavorite() {
    console.log('addToFavorite');
  }
}
