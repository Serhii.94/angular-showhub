import { Component } from '@angular/core';

import { UI_CONSTANTS } from 'src/app/constants';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss'],
})
export class LogoComponent {
  UI = UI_CONSTANTS;
}
