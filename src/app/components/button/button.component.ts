import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  @Input() buttonText: string = 'Button';
  @Input() buttonType?: string = 'button';
  @Output() onClick = new EventEmitter<void>();
}
