import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { NavigationService } from 'src/app/services';

import { NavigationItem } from 'src/app/types';

import { ROUTES, UI_CONSTANTS } from 'src/app/constants';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent {
  routes = ROUTES;
  UI = UI_CONSTANTS;
  dynamicNavigation: NavigationItem[] = [];
  currentId: string;

  constructor(
    private router: Router,
    public navigationService: NavigationService,
  ) {
    this.router.events.subscribe(() => {
      this.dynamicNavigation = this.navigationService.getNavItems();
      this.currentId = this.navigationService.currentId;
    });
  }
}
