import { Component, Input, Output, EventEmitter } from '@angular/core';

import { PAGINATION } from 'src/app/constants';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent {
  @Input() currentPage = PAGINATION.CURR_PAGE;
  @Output() pageChanged = new EventEmitter<number>();

  onPageChanged(pageNumber: number) {
    this.currentPage = pageNumber;
    this.pageChanged.emit(pageNumber);
  }
}
