import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-animated-title',
  templateUrl: './animated-title.component.html',
  styleUrls: ['./animated-title.component.scss'],
})
export class AnimatedTitleComponent implements OnChanges, OnInit {
  @Input() inputWord: string = '';
  @Output() isVisible = new EventEmitter<boolean>();

  iterateWord: Array<string>;

  ngOnChanges(changes: SimpleChanges) {
    if (changes.inputWord) {
      this.iterateWord = this.inputWord.split('');
    }
  }

  ngOnInit(): void {
    this.isVisible.emit(true);
  }
}
