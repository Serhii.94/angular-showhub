import { Component, Input } from '@angular/core';

import { IShow } from 'src/app/models';

import { MAIN_PATH, ROUTES, UI_CONSTANTS } from 'src/app/constants';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss'],
})
export class ShowComponent {
  UI = UI_CONSTANTS;
  routes = ROUTES;
  main = MAIN_PATH;

  @Input() show: IShow;

  addToFavorite(id: string) {
    console.log('Add to favorite show with id:', id);
  }
}
