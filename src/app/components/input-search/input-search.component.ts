import { Component } from '@angular/core';

import { SearchService, SearchModeService } from 'src/app/services';

import { SearchMode } from 'src/app/types';

import { UI_CONSTANTS } from 'src/app/constants';

@Component({
  selector: 'app-input-search',
  templateUrl: './input-search.component.html',
  styleUrls: ['./input-search.component.scss'],
})
export class InputSearchComponent {
  UI = UI_CONSTANTS;
  term: string;
  searchMode: SearchMode;

  constructor(
    private searchService: SearchService,
    private searchModeService: SearchModeService,
  ) {}

  handleSearch(event: KeyboardEvent) {
    this.searchMode = this.searchModeService.getSearchMode();

    const query = this.term;

    if (event.key === 'Enter' && query) {
      this.searchService.search(query, this.searchMode).subscribe((results) => {
        this.searchService.searchResults = results;
      });
    }
  }
}
