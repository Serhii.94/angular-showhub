import { IActor, IShow } from '../models';

export type ShowOrPerson = IShow | IActor;

export type SearchMode = 'shows' | 'actors';

export enum SearchModeEnum {
  shows = 'shows',
  actors = 'actors',
}

export type NavigationItem = {
  path: string;
  name: string;
  categoryPath: string;
};

export type User = {
  name?: string | null;
  email?: string | null;
  registeredSince?: string | null;
  photo?: string | null;
};
