import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//?------------------------------------[Firebase]
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import {
  provideAnalytics,
  getAnalytics,
  ScreenTrackingService,
  UserTrackingService,
} from '@angular/fire/analytics';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { provideDatabase, getDatabase } from '@angular/fire/database';
import { provideFirestore, getFirestore } from '@angular/fire/firestore';
import { provideFunctions, getFunctions } from '@angular/fire/functions';
import { provideMessaging, getMessaging } from '@angular/fire/messaging';
import { providePerformance, getPerformance } from '@angular/fire/performance';
import {
  provideRemoteConfig,
  getRemoteConfig,
} from '@angular/fire/remote-config';
import { provideStorage, getStorage } from '@angular/fire/storage';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
//?------------------------------------[Pages]
import {
  HomePageComponent,
  ShowsPageComponent,
  ShowMainPageComponent,
  ShowSeasonsPageComponent,
  ShowCastPageComponent,
  ShowCrewPageComponent,
  ShowGalleryPageComponent,
  ActorsPageComponent,
  ProfilePageComponent,
  ActorMainPageComponent,
} from './pages';
//?------------------------------------[Components]
import { AppComponent } from './app.component';
import {
  FooterComponent,
  ButtonComponent,
  InputSearchComponent,
  LogoComponent,
  NavigationComponent,
  HeaderComponent,
  ShowComponent,
  ActorComponent,
  ModalComponent,
  SpinnerComponent,
  PaginationComponent,
  RandomShowsComponent,
  AnimatedTitleComponent,
  HomeLinkComponent,
  NotYetAvailableComponent,
} from './components';

//?------------------------------------[Pipes, Directives]
import { YearPipe, GetAgePipe, FilterAndSortImagesPipe } from './pipes';
import { FocusDirective } from './directives';

import { environment } from '../environments';

@NgModule({
  declarations: [
    ShowComponent,
    ActorComponent,
    AppComponent,
    LogoComponent,
    ButtonComponent,
    NavigationComponent,
    FooterComponent,
    HeaderComponent,
    ModalComponent,
    SpinnerComponent,
    InputSearchComponent,
    PaginationComponent,
    AnimatedTitleComponent,
    HomeLinkComponent,

    HomePageComponent,
    ShowsPageComponent,
    ShowMainPageComponent,
    ShowSeasonsPageComponent,
    ShowCastPageComponent,
    ShowCrewPageComponent,
    ShowGalleryPageComponent,
    RandomShowsComponent,

    ActorsPageComponent,
    ActorMainPageComponent,
    ProfilePageComponent,

    FilterAndSortImagesPipe,
    YearPipe,
    GetAgePipe,

    FocusDirective,
    NotYetAvailableComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,

    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideAnalytics(() => getAnalytics()),
    provideAuth(() => getAuth()),
    provideDatabase(() => getDatabase()),
    provideFirestore(() => getFirestore()),
    provideFunctions(() => getFunctions()),
    provideMessaging(() => getMessaging()),
    providePerformance(() => getPerformance()),
    provideRemoteConfig(() => getRemoteConfig()),
    provideStorage(() => getStorage()),
  ],
  providers: [ScreenTrackingService, UserTrackingService],
  bootstrap: [AppComponent],
})
export class AppModule {}
