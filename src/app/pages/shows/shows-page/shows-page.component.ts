import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {
  ShowsService,
  SearchService,
  NavigationService,
} from 'src/app/services';

import { IShow } from 'src/app/models';

import { fromTheDepths } from 'src/app/utils';

import { validateShow } from 'src/app/utils';

import { UI_CONSTANTS, PAGINATION, ROUTES } from 'src/app/constants';

@Component({
  selector: 'app-shows-page',
  templateUrl: './shows-page.component.html',
  styleUrls: ['./shows-page.component.scss'],
  animations: [fromTheDepths],
})
export class ShowsPageComponent implements OnInit {
  showsList: IShow[] = [];
  isLoading = false;
  isDisabled = false;
  isContentVisible = false;

  UI = UI_CONSTANTS;
  currentPage = PAGINATION.CURR_PAGE;
  itemsPerPage = PAGINATION.ITEMS_PER_PAGE;
  showsRoute = ROUTES.SHOWS;
  pageNumber: number;
  isShowValid = validateShow;

  constructor(
    public showsService: ShowsService,
    public searchService: SearchService,
    private navigationService: NavigationService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.isLoading = true;

    this.showsService.getShowsList().subscribe((responsShows: IShow[]) => {
      this.showsList = responsShows;
      this.isLoading = false;
    });

    this.navigationService.smoothTopScrolling();
  }

  handleRandomShows() {
    this.isLoading = true;
    this.isDisabled = true;
    this.currentPage = PAGINATION.CURR_PAGE;

    const randomPage = Math.floor(Math.random() * PAGINATION.MAX_PAGES) + 1;

    setTimeout(() => {
      this.isDisabled = false;
    }, 5000);

    this.showsService
      .getRandomShowsList(randomPage)
      .subscribe((responsShows: IShow[]) => {
        this.showsList = responsShows;
        this.isLoading = false;
      });
  }

  handleVisible(isVisible: boolean) {
    setTimeout(() => {
      this.isContentVisible = isVisible;
    }, 200);
  }

  handlePageChange(pageNumber: number) {
    this.currentPage = pageNumber;
    this.router.navigate([this.showsRoute], {
      queryParams: { page: pageNumber },
    });
  }
}
