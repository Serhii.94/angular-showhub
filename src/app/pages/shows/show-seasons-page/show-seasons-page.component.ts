import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ShowsService } from 'src/app/services';

import { IShowSeason } from 'src/app/models';

@Component({
  selector: 'app-show-seasons-page',
  templateUrl: './show-seasons-page.component.html',
  styleUrls: ['./show-seasons-page.component.scss'],
})
export class ShowSeasonsPageComponent implements OnInit {
  showSeasons?: IShowSeason[] = [];
  isLoading = false;

  constructor(
    private showsService: ShowsService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    const showId = this.route.snapshot.params['id'];
    this.isLoading = true;

    this.showsService
      .getShowsSeasons(showId)
      .subscribe((seasons: IShowSeason[]) => {
        this.showSeasons = seasons;
        this.isLoading = false;
      });
  }
}
