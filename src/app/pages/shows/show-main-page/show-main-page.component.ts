import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ShowsService } from 'src/app/services';

import { IShow } from 'src/app/models';

import { UI_CONSTANTS } from 'src/app/constants';

@Component({
  selector: 'app-show-main-page',
  templateUrl: './show-main-page.component.html',
  styleUrls: ['./show-main-page.component.scss'],
})
export class ShowMainPageComponent implements OnInit {
  show?: IShow;
  isLoading = false;
  UI = UI_CONSTANTS;

  constructor(
    private showsService: ShowsService,
    private route: ActivatedRoute,
  ) {}

  addToFavorite(id?: string) {
    if (id) {
      console.log('Add to favorite show with id:', id);
    }
  }

  ngOnInit() {
    const showId = this.route.snapshot.params['id'];
    this.isLoading = true;

    this.showsService.getShowById(showId).subscribe((show: IShow) => {
      this.show = show;
      this.isLoading = false;
    });
  }
}
