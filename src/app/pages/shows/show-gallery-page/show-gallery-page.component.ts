import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ShowsService } from 'src/app/services';

import { IShowImage } from 'src/app/models';

import { UI_CONSTANTS } from 'src/app/constants';

@Component({
  selector: 'app-show-gallery-page',
  templateUrl: './show-gallery-page.component.html',
  styleUrls: ['./show-gallery-page.component.scss'],
})
export class ShowGalleryPageComponent implements OnInit {
  UI = UI_CONSTANTS;

  posterImages: IShowImage[] = [];
  backgroundImages: IShowImage[] = [];
  typographyImages: IShowImage[] = [];
  bannerImages: IShowImage[] = [];
  isLoading = false;

  constructor(
    private showsService: ShowsService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    const showId = this.route.snapshot.params['id'];
    this.isLoading = true;

    this.showsService
      .getShowsImages(showId)
      .subscribe((images: IShowImage[]) => {
        this.posterImages = images.filter((img) => img.type === 'poster');
        this.backgroundImages = images.filter(
          (img) => img.type === 'background',
        );
        this.typographyImages = images.filter(
          (img) => img.type === 'typography',
        );
        this.bannerImages = images.filter((img) => img.type === 'banner');
        this.isLoading = false;
      });
  }
}
