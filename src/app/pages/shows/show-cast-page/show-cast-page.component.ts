import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ShowsService } from 'src/app/services';

import { IShowCast } from 'src/app/models';

import { MAIN_PATH, ROUTES } from 'src/app/constants';

@Component({
  selector: 'app-show-cast-page',
  templateUrl: './show-cast-page.component.html',
  styleUrls: ['./show-cast-page.component.scss'],
})
export class ShowCastPageComponent implements OnInit {
  showCasts?: IShowCast[] = [];
  isLoading = false;
  routes = ROUTES;
  main = MAIN_PATH;

  constructor(
    private showsService: ShowsService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    const showId = this.route.snapshot.params['id'];
    this.isLoading = true;

    this.showsService.getShowsCast(showId).subscribe((casts: IShowCast[]) => {
      this.showCasts = casts;
      this.isLoading = false;
    });
  }
}
