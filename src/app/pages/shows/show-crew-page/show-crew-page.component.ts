import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ShowsService } from 'src/app/services';

import { ICrew } from 'src/app/models';

import { MAIN_PATH, ROUTES } from 'src/app/constants';

@Component({
  selector: 'app-show-crew-page',
  templateUrl: './show-crew-page.component.html',
  styleUrls: ['./show-crew-page.component.scss'],
})
export class ShowCrewPageComponent implements OnInit {
  crews?: ICrew[] = [];
  isLoading = false;
  routes = ROUTES;
  main = MAIN_PATH;

  constructor(
    private showsService: ShowsService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    const showId = this.route.snapshot.params['id'];
    this.isLoading = true;

    this.showsService
      .getShowsCrew(showId)
      .subscribe((responseCrews: ICrew[]) => {
        this.crews = responseCrews;
        this.isLoading = false;
      });
  }
}
