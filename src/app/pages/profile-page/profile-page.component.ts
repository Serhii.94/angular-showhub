import { Component, OnInit } from '@angular/core';

import { AuthService } from 'src/app/services';

import { formatDate } from 'src/app/utils';

import { IShow } from 'src/app/models';

import { DEFAULT_USER_DATA, ROUTES, UI_CONSTANTS } from 'src/app/constants';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
})
export class ProfilePageComponent implements OnInit {
  routes = ROUTES;
  UI = UI_CONSTANTS;
  userData = DEFAULT_USER_DATA;
  isShowsVisible = false;
  isActorsVisible = false;

  likedShows: IShow[] = [];

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.authService.getUser().subscribe((user) => {
      if (user) {
        const {
          displayName,
          email,
          photoURL,
          metadata: { creationTime },
        } = user;

        this.userData.name = displayName;
        this.userData.email = email;
        this.userData.photo = photoURL;
        this.userData.registeredSince = formatDate(creationTime);
      }
    });
  }

  showShows() {
    this.isShowsVisible = true;
  }

  hideShows() {
    this.isShowsVisible = false;
  }
}
