import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {
  SearchService,
  ActorsService,
  NavigationService,
} from 'src/app/services';

import { IActor } from 'src/app/models';

import { fromTheDepths } from 'src/app/utils';

import { validateActor } from 'src/app/utils';

import { UI_CONSTANTS, PAGINATION, ROUTES } from 'src/app/constants';

@Component({
  selector: 'app-actors-page',
  templateUrl: './actors-page.component.html',
  styleUrls: ['./actors-page.component.scss'],
  animations: [fromTheDepths],
})
export class ActorsPageComponent implements OnInit {
  actorsList?: IActor[] = [];
  isLoading = false;
  isContentVisible = false;

  UI = UI_CONSTANTS;
  currentPage = PAGINATION.CURR_PAGE;
  itemsPerPage = PAGINATION.ITEMS_PER_PAGE;
  actorsRoute = ROUTES.ACTORS;
  isActorValid = validateActor;

  constructor(
    public actorsService: ActorsService,
    public searchService: SearchService,
    private navigationService: NavigationService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.isLoading = true;

    this.actorsService.getActorsList().subscribe((response: IActor[]) => {
      this.actorsList = response;
      this.isLoading = false;
    });

    this.navigationService.smoothTopScrolling();
  }

  handleVisible(isVisible: boolean) {
    setTimeout(() => {
      this.isContentVisible = isVisible;
    }, 200);
  }

  handlePageChange(pageNumber: number) {
    this.currentPage = pageNumber;
    this.router.navigate([this.actorsRoute], {
      queryParams: { page: pageNumber },
    });
  }
}
