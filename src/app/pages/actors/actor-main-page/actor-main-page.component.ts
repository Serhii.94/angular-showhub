import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ActorsService } from 'src/app/services';

import { IActor } from 'src/app/models';

import { UI_CONSTANTS } from 'src/app/constants';

@Component({
  selector: 'app-actor-main-page',
  templateUrl: './actor-main-page.component.html',
  styleUrls: ['./actor-main-page.component.scss'],
})
export class ActorMainPageComponent implements OnInit {
  actor?: IActor;
  isLoading = false;
  UI = UI_CONSTANTS;

  constructor(
    private actorsService: ActorsService,
    private route: ActivatedRoute,
  ) {}

  addToFavorite(id?: string) {
    if (id) {
      console.log('Add to favorite actor with id:', id);
    }
  }

  ngOnInit() {
    const showId = this.route.snapshot.params['id'];
    this.isLoading = true;

    this.actorsService.getActorById(showId).subscribe((actor: IActor) => {
      this.actor = actor;
      this.isLoading = false;
    });
  }
}
