import { IActor, IShow } from 'src/app/models';

import { NavigationItem, User } from '../types';

export const URL_CONSTANTS = {
  SHOWS_URL: 'https://api.tvmaze.com/shows',
  ACTORS_URL: 'https://api.tvmaze.com/people',
  SEARCH_SHOWS_URL: 'https://api.tvmaze.com/search/shows',
  SEARCH_ACTORS_URL: 'https://api.tvmaze.com/search/people',
};

export const UI_CONSTANTS = {
  txt_shows: 'Shows',
  txt_following_shows: 'Following Shows',
  txt_actors: 'Actors',
  txt_login: 'Google login',
  txt_logout: 'Log out',
  txt_login_with: 'Login with',
  txt_login_via_google: 'Login via google account',
  txt_exit: 'Exit',
  txt_random: 'Random',
  txt_cabinet: 'Cabinet',
  txt_username: 'Username',
  txt_user_photo: `User's Photo`,
  txt_registered_since: 'Registered since:',
  txt_search_shows_and_actors: 'Search Shows and Actors',
  txt_click_to_follow: 'Click to follow',
  txt_average_rating: 'Average rating',

  txt_hide_shows: 'Hide shows',
  txt_show_shows: 'Show shows',

  txt_posters: 'Posters:',
  txt_banners: 'Banners:',
  txt_backgrounds: 'Backgrounds:',
  txt_typographies: 'Typographies:',

  random_shows_txt_1:
    'You have the ability to locate any show by utilizing the search field',
  random_shows_txt_2:
    'Should you desire to see additional shows, simply click the Random (available every 5 seconds)',
  random_shows_txt_3:
    'We have curated a compilation of shows that are recommended for you',
};

export const PAGINATION = {
  CURR_PAGE: 1,
  ITEMS_PER_PAGE: 49,
  MAX_PAGES: 50,
};

export const SHOW_NAVIGATION_ITEMS: NavigationItem[] = [
  { categoryPath: 'shows/', path: 'main', name: 'Main' },
  { categoryPath: 'shows/', path: 'seasons', name: 'Seasons' },
  { categoryPath: 'shows/', path: 'cast', name: 'Cast' },
  { categoryPath: 'shows/', path: 'crew', name: 'Crew' },
  { categoryPath: 'shows/', path: 'gallery', name: 'Gallery' },
];

export const ACTOR_NAVIGATION_ITEMS: NavigationItem[] = [
  { categoryPath: 'actors/', path: 'main', name: 'Main' },
];

export const ROUTES = {
  HOME: '/home',
  SHOWS: '/shows',
  ACTORS: '/actors',
  PROFILE: '/profile',
};

export const MAIN_PATH = 'main';

export const HOME_LINKS_LIST = [
  {
    title: 'Shows',
    routerLink: '/shows',
    imgPath: 'assets/images/series.png',
  },
  {
    title: 'Actors',
    routerLink: '/actors',
    imgPath: 'assets/images/actors.png',
  },
];

export const PATTERNS = {
  email: /^[^\s@]+@[^\s@]+\.[a-zA-Z]{2,}$/,
  password: /^(?=.*d)(?=.*[a-z])(?=.*[A-Z]).{8,16}$/,
};

export const DEFAULT_USER_DATA: User = {
  name: '',
  email: '',
  photo: '',
  registeredSince: '',
};

export const DEFAULT_SHOW: IShow = {
  id: '',
  url: '',
  name: '',
  type: '',
  language: '',
  genres: [],
  status: '',
  runtime: 0,
  averageRuntime: 0,
  premiered: '',
  ended: '',
  officialSite: '',
  schedule: {
    time: '',
    days: [],
  },
  rating: {
    average: 0,
  },
  weight: 0,
  network: {
    id: '',
    name: '',
    country: {
      name: '',
      code: '',
      timezone: '',
    },
    officialSite: '',
  },
  webChannel: null,
  dvdCountry: null,
  externals: {
    tvrage: 0,
    thetvdb: 0,
    imdb: '',
  },
  image: {
    medium: '',
    original: '',
  },
  summary: '',
  updated: 0,
  _links: {
    self: {
      href: '',
    },
    previousepisode: {
      href: '',
    },
  },
};

export const DEFAULT_ACTOR: IActor = {
  id: '',
  url: '',
  name: '',
  country: {
    name: '',
    code: '',
    timezone: '',
  },
  birthday: '',
  deathday: '',
  gender: '',
  image: {
    medium: '',
    original: '',
  },
  updated: 0,
  _links: {
    self: {
      href: '',
    },
  },
};
