export interface ICrew {
  type: CrewType;
  person: {
    id: string;
    url: string;
    name: string;
    country: {
      name: string;
      code: string;
      timezone: string;
    };
    birthday: string;
    deathday: string;
    gender: string;
    image: {
      medium: string;
      original: string;
    };
    updated: number;
    _links: {
      self: {
        href: string;
      };
    };
  };
}

type CrewType =
  | 'Producer'
  | 'Executive Producer'
  | 'Co-Executive Producer'
  | 'Director Of Photography'
  | 'Co-Producer'
  | 'Story Editor'
  | 'Supervising Producer'
  | 'Associate Producer'
  | 'Consulting Producer'
  | 'Consultant'
  | 'Executive Story Editor'
  | 'Developer'
  | 'Unit Production Manager'
  | 'Second Unit Director'
  | 'Casting'
  | 'Music Supervisor'
  | 'Casting Associate'
  | 'First Assistant Director'
  | 'Re-Recording Mixer'
  | 'Visual Effects Supervisor'
  | 'Costume Designer'
  | 'Production Designer'
  | 'Music Editor'
  | 'Supervising Sound Editor'
  | 'Second Assistant Director'
  | 'Main Title Theme'
  | 'Art Director'
  | 'Sound Supervisor'
  | 'Music';
