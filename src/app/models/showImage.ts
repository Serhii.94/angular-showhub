export interface IShowImage {
  id: string;
  type: 'poster' | 'background' | 'typography' | 'banner';
  main: boolean;
  resolutions: {
    original: {
      url: string;
      width: number;
      height: number;
    };
    medium: {
      url: string;
      width: number;
      height: number;
    };
  };
}
