export type Gender = 'Male' | 'Female' | '';

export interface IActor {
  id: string;
  url: string;
  name: string;
  country: {
    name: string;
    code: string;
    timezone: string;
  };
  birthday: string;
  deathday: string;
  gender: Gender;
  image: {
    medium?: string;
    original: string;
  };
  updated: number;
  _links: {
    self: {
      href?: string;
    };
  };
}
