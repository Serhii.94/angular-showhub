import { Gender } from './actor';

export interface IUser {
  id?: string;
  email?: string;
  username?: string;
  birthday?: string;
  gender?: Gender;
  countryName?: string;
  photo?: string;
}
