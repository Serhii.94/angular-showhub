export type WeekDays =
  | 'Monday'
  | 'Tuesday'
  | 'Wednesday'
  | 'Thursday'
  | 'Friday'
  | 'Saturday'
  | 'Sunday';

export type Genres =
  | 'Action'
  | 'Adventure'
  | 'Comedy'
  | 'Drama'
  | 'Horror'
  | 'Mystery'
  | 'Romance'
  | 'Science-Fiction'
  | 'Thriller'
  | 'Crime'
  | 'Horror'
  | 'Espionage'
  | 'Music'
  | 'Mystery'
  | 'Supernatural'
  | 'Fantasy'
  | 'Family'
  | 'Anime'
  | 'History'
  | 'Medical'
  | 'Legal';

export interface IShow {
  id: string;
  url: string;
  name: string;
  type: string;
  language: string;
  genres: Genres[];
  status: string;
  runtime: number;
  averageRuntime: number;
  premiered: string;
  ended: string;
  officialSite: string;
  schedule: {
    time: string;
    days: WeekDays[];
  };
  rating: {
    average: number;
  };
  weight: number;
  network: {
    id: string;
    name: string;
    country: {
      name: string;
      code: string;
      timezone: string;
    };
    officialSite: string;
  };
  webChannel: null;
  dvdCountry: null;
  externals: {
    tvrage: number;
    thetvdb: number;
    imdb: string;
  };
  image: {
    medium: string;
    original: string;
  };
  summary: string;
  updated: number;
  _links: {
    self: {
      href: string;
    };
    previousepisode: {
      href: string;
    };
  };
}
