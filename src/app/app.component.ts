import { Component } from '@angular/core';

import { NavigationService } from './services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  constructor(public navigationService: NavigationService) {}
}
