import { Injectable } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/compat/auth';
import { GoogleAuthProvider } from 'firebase/auth';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private fireAuth: AngularFireAuth) {}

  async login(email: string, password: string) {
    try {
      return await this.fireAuth.signInWithEmailAndPassword(email, password);
    } catch (err) {
      console.log(err);
      return null;
    }
  }

  async googleLogIn() {
    try {
      return await this.fireAuth.signInWithPopup(new GoogleAuthProvider());
    } catch (err) {
      console.log(err);
      return null;
    }
  }

  async logOut() {
    await this.fireAuth.signOut();
  }

  getUser() {
    return this.fireAuth.authState;
  }
}

/*
import { useAuthState } from "react-firebase-hooks/auth";
import firebase from "firebase";
?----------------------------------------------------------------
const auth = firebase.auth();
auth.signOut();
?----------------------------------------------------------------
const { auth } = useContext(Context);
const [user] = useAuthState(auth);
?----------------------------------------------------------------
try {
	const userId = firebase.auth().currentUser.uid;
	const show = database.ref(`likedFilms/user-${userId}/`);
	show.on('value', el => {
		setShow(el.val());
	});
} catch {
	console.log('Error');
}
*/
