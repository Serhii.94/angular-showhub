import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { NavigationItem } from 'src/app/types';

import {
  SHOW_NAVIGATION_ITEMS,
  ACTOR_NAVIGATION_ITEMS,
  ROUTES,
} from 'src/app/constants';

@Injectable({
  providedIn: 'root',
})
export class NavigationService {
  dynamicNavigation: NavigationItem[] = [];
  showsNavigation = SHOW_NAVIGATION_ITEMS;
  actorNavigation = ACTOR_NAVIGATION_ITEMS;
  currentId: string;

  private homeRoute = ROUTES.HOME;
  private profileRoute = ROUTES.PROFILE;

  showHeadeFooterNavigation: boolean;
  showSearchNavigation: boolean;

  constructor(private router: Router) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.setRouteConfig(event.url);
      }
    });
  }

  private setRouteConfig(url: string): void {
    this.showHeadeFooterNavigation = !url.includes(this.profileRoute);
    this.showSearchNavigation = !url.includes(this.homeRoute);
  }

  redirectToHome() {
    this.router.navigate([this.homeRoute]);
  }

  smoothTopScrolling() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        window.scrollTo({ top: 0, behavior: 'smooth' });
      }
    });
  }

  getNavItems() {
    let currentRoute = this.router.routerState.snapshot.root;
    let isExitLoop = false;
    while (!isExitLoop) {
      if (currentRoute.children && currentRoute.children.length) {
        currentRoute = currentRoute.children[0];
      } else if (
        currentRoute.data &&
        currentRoute.data.navItem &&
        currentRoute.url[0].path === 'shows'
      ) {
        this.dynamicNavigation = this.showsNavigation;
        isExitLoop = true;
      } else if (
        currentRoute.data &&
        currentRoute.data.navItem &&
        currentRoute.url[0].path === 'actors'
      ) {
        this.dynamicNavigation = this.actorNavigation;
        isExitLoop = true;
      } else {
        this.dynamicNavigation = [];
        isExitLoop = true;
      }
    }
    this.currentId = currentRoute.params.id;
    return this.dynamicNavigation;
  }
}
