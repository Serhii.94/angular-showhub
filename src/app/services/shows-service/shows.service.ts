import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, ReplaySubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';

import {
  IShow,
  ICrew,
  IShowImage,
  IShowSeason,
  IShowCast,
} from 'src/app/models';

import { URL_CONSTANTS as URL } from 'src/app/constants';

@Injectable({
  providedIn: 'root',
})
export class ShowsService {
  private showsListCache$: ReplaySubject<IShow[]> = new ReplaySubject<IShow[]>(
    1,
  );
  private randomShowsListCache$: ReplaySubject<IShow[]> = new ReplaySubject<
    IShow[]
  >(1);

  constructor(private http: HttpClient) {}

  getShowsList(): Observable<IShow[]> {
    if (!this.showsListCache$.observed) {
      const url = URL.SHOWS_URL;
      const request$ = this.http.get<IShow[]>(url).pipe(shareReplay(1));
      request$.subscribe(this.showsListCache$);
      return request$;
    }
    return this.showsListCache$;
  }

  getRandomShowsList(page = 1): Observable<IShow[]> {
    if (!this.randomShowsListCache$.observed) {
      const url = `${URL.SHOWS_URL}?page=${page}`;
      const request$ = this.http.get<IShow[]>(url).pipe(
        map((shows: IShow[]) =>
          shows.filter((show: IShow) =>
            Boolean(show.image && show.image.medium),
          ),
        ),
        shareReplay(1),
      );
      request$.subscribe(this.randomShowsListCache$);
      return request$;
    }
    return this.randomShowsListCache$;
  }

  getShowById(id: string): Observable<IShow> {
    const url = `${URL.SHOWS_URL}/${id}`;
    return this.http.get<IShow>(url);
  }

  getShowsSeasons(id: string): Observable<IShowSeason[]> {
    const url = `${URL.SHOWS_URL}/${id}/seasons`;
    return this.http.get<IShowSeason[]>(url);
  }

  getShowsCast(id: string): Observable<IShowCast[]> {
    const url = `${URL.SHOWS_URL}/${id}/cast`;
    return this.http.get<IShowCast[]>(url);
  }

  getShowsCrew(id: string): Observable<ICrew[]> {
    const url = `${URL.SHOWS_URL}/${id}/crew`;
    return this.http.get<ICrew[]>(url);
  }

  getShowsImages(id: string): Observable<IShowImage[]> {
    const url = `${URL.SHOWS_URL}/${id}/images`;
    return this.http.get<IShowImage[]>(url);
  }
}
