import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { SearchMode, ShowOrPerson } from 'src/app/types';

import { IShow, IActor } from 'src/app/models';

import { getEndpointBySearchMode } from 'src/app/utils';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  searchResults: ShowOrPerson[] = [];

  constructor(private http: HttpClient) {}

  search(
    query: string | false,
    searchMode: SearchMode,
  ): Observable<ShowOrPerson[]> {
    if (query) {
      const endpoint = getEndpointBySearchMode(searchMode, query);
      return this.http.get<{ show?: IShow; actor?: IActor }[]>(endpoint).pipe(
        map((results) => {
          return (this.searchResults = results
            .map((result) => {
              if (result.hasOwnProperty('show')) {
                return result.show;
              } else if (result.hasOwnProperty('actor')) {
                return result.actor;
              } else {
                return null;
              }
            })
            .filter((result): result is ShowOrPerson => Boolean(result)));
        }),
      );
    }
    return of([]);
  }
}
