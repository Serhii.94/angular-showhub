import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { SearchMode, SearchModeEnum } from 'src/app/types';

@Injectable({
  providedIn: 'root',
})
export class SearchModeService {
  private defaultSearchMode: SearchMode = SearchModeEnum.shows;

  constructor(private router: Router) {}

  getSearchMode(): SearchMode {
    const urlSegment = this.router?.routerState?.snapshot?.url;
    const urlSegmentPath = urlSegment.split('/')[1];

    if (!urlSegment) {
      console.error('SearchModeService: URL segment not found');
      return this.defaultSearchMode;
    }

    switch (urlSegmentPath) {
      case SearchModeEnum.actors:
        return SearchModeEnum.actors;
      default:
        return SearchModeEnum.shows;
    }
  }
}
