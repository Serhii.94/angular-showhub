import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, ReplaySubject, map } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

import { IActor } from 'src/app/models';

import { URL_CONSTANTS as URL } from 'src/app/constants';

@Injectable({
  providedIn: 'root',
})
export class ActorsService {
  private actorsListCache$: ReplaySubject<IActor[]> = new ReplaySubject<
    IActor[]
  >(1);

  constructor(private http: HttpClient) {}

  getActorsList(): Observable<IActor[]> {
    if (!this.actorsListCache$.observed) {
      const url = URL.ACTORS_URL;
      const request$ = this.http.get<IActor[]>(url).pipe(
        map((actors: IActor[]) =>
          actors.filter((actorItem: IActor) => Boolean(actorItem.gender)),
        ),
        shareReplay(1),
      );
      request$.subscribe(this.actorsListCache$);
      return request$;
    }
    return this.actorsListCache$;
  }

  getActorById(id: string): Observable<IActor> {
    const url = `${URL.ACTORS_URL}/${id}`;
    return this.http.get<IActor>(url);
  }

  getActorsImages(id: string): Observable<IActor[]> {
    const url = `${URL.ACTORS_URL}/${id}/images`;
    return this.http.get<IActor[]>(url);
  }
}
