import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  loginModalIsVisible$ = new BehaviorSubject<boolean>(false);

  openLoginModal() {
    this.loginModalIsVisible$.next(true);
  }

  close() {
    this.loginModalIsVisible$.next(false);
  }
}
