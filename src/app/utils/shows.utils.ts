import { IShow } from 'src/app/models';

import { DEFAULT_SHOW } from 'src/app/constants';

export const validateShow = (obj: unknown): IShow => {
  const show = obj as Partial<IShow>;
  if (show.genres) {
    return show as IShow;
  } else {
    return DEFAULT_SHOW;
  }
};
