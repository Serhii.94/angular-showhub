import { IActor } from 'src/app/models';

import { DEFAULT_ACTOR } from '../constants';

export const validateActor = (obj: unknown): IActor => {
  const actor = obj as Partial<IActor>;
  if (actor.gender) {
    return actor as IActor;
  } else {
    return DEFAULT_ACTOR;
  }
};
