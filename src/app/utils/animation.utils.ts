import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

export const fadeInOut = trigger('fadeInOut', [
  state(
    'visible',
    style({
      opacity: 1,
      transform: 'scale(1) translateX(0%)',
    }),
  ),
  state(
    'hidden',
    style({
      opacity: 0,
      transform: 'scale(1) translateX(-100%)',
    }),
  ),
  transition('visible => hidden', animate('800ms')),
  transition('hidden => visible', animate('1200ms')),
]);

export const fromTheDepths = trigger('fromTheDepths', [
  state(
    'visible',
    style({
      opacity: 1,
      transform: 'scale(1) translateX(0%)',
    }),
  ),
  state(
    'hidden',
    style({
      opacity: 0,
      transform: 'scale(1) translateY(100%)',
    }),
  ),
  transition('visible => hidden', animate('0ms')),
  transition('hidden => visible', animate('2000ms')),
]);
