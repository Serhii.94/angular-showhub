import { SearchMode } from 'src/app/types';
import { URL_CONSTANTS as APP } from 'src/app/constants';

export const getEndpointBySearchMode = (
  searchMode: SearchMode,
  query: string | false,
) => {
  return searchMode === 'shows'
    ? `${APP.SEARCH_SHOWS_URL}?q=${query}`
    : `${APP.SEARCH_ACTORS_URL}?q=${query}`;
};
