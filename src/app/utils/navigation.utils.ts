import { NavigationEnd, Router } from '@angular/router';

import { NavigationItem } from 'src/app/types';
import {
  SHOW_NAVIGATION_ITEMS,
  ACTOR_NAVIGATION_ITEMS,
} from 'src/app/constants';

let router: Router;

export const setNavigationType = () => {
  let navigation: NavigationItem[] = [];
  const showsNavigation = SHOW_NAVIGATION_ITEMS;
  const actorNavigation = ACTOR_NAVIGATION_ITEMS;
  let currentId: string;

  router.events.subscribe((event: any) => {
    if (event instanceof NavigationEnd) {
      let currentRoute = router.routerState.snapshot.root;
      let isExitLoop = false;
      while (!isExitLoop) {
        if (currentRoute.children && currentRoute.children.length) {
          currentRoute = currentRoute.children[0];
        } else if (
          currentRoute.data &&
          currentRoute.data.navItem &&
          currentRoute.url[0].path === 'shows'
        ) {
          navigation = showsNavigation;
          isExitLoop = true;
        } else if (
          currentRoute.data &&
          currentRoute.data.navItem &&
          currentRoute.url[0].path === 'actors'
        ) {
          navigation = actorNavigation;
          isExitLoop = true;
        } else {
          navigation = [];
          isExitLoop = true;
        }
      }
      currentId = currentRoute.params.id;
    }
  });
};
