export const formatDate = (dateString?: string | null): string => {
  const dateLength = 4;
  if (dateString) {
    const arrFromDateString = dateString.split(' ');
    return arrFromDateString.slice(0, dateLength).join(' ');
  }
  return 'It is unknown!';
};
