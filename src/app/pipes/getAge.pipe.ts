import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getAge',
})
export class GetAgePipe implements PipeTransform {
  transform(value?: string): number | string {
    if (value) {
      const birthDate = new Date(value);
      const now = new Date();
      const diffInMs = now.getTime() - birthDate.getTime();
      const msInYear = 1000 * 60 * 60 * 24 * 365.25;
      const diffInYears = diffInMs / msInYear;
      return Math.floor(diffInYears);
    }
    return '';
  }
}
