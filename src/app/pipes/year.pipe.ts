import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'year',
})
export class YearPipe implements PipeTransform {
  transform(value?: string): string | number {
    if (value) {
      const date = new Date(value);
      return date.getFullYear();
    }
    return '';
  }
}
