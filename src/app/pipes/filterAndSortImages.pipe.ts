import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterAndSortImages',
})
export class FilterAndSortImagesPipe<T extends { name: string; image: any }>
  implements PipeTransform
{
  transform(data: T[]): T[] {
    const filteredData = data.filter(
      (item) => item.image !== undefined && item.image !== null,
    );
    const sortedData = filteredData.sort((a, b) =>
      a.image < b.image ? -1 : 1,
    );
    return sortedData.concat(filteredData);
  }
}
