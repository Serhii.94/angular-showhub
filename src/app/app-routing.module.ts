import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
  HomePageComponent,
  ShowsPageComponent,
  ActorMainPageComponent,
  ProfilePageComponent,
  ActorsPageComponent,
  ShowGalleryPageComponent,
  ShowCrewPageComponent,
  ShowCastPageComponent,
  ShowSeasonsPageComponent,
  ShowMainPageComponent,
} from './pages';

const routes: Routes = [
  { path: 'home', component: HomePageComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'shows',
    component: ShowsPageComponent,
    children: [
      {
        path: ':pageNumber',
        component: ShowsPageComponent,
      },
    ],
  },
  {
    path: 'shows/:id/main',
    component: ShowMainPageComponent,
    data: { navItem: 'Main' },
  },
  {
    path: 'shows/:id/seasons',
    component: ShowSeasonsPageComponent,
    data: { navItem: 'Seasons' },
  },
  {
    path: 'shows/:id/cast',
    component: ShowCastPageComponent,
    data: { navItem: 'Cast' },
  },
  {
    path: 'shows/:id/crew',
    component: ShowCrewPageComponent,
    data: { navItem: 'Crew' },
  },
  {
    path: 'shows/:id/gallery',
    component: ShowGalleryPageComponent,
    data: { navItem: 'Gallery' },
  },
  {
    path: 'actors',
    component: ActorsPageComponent,
  },
  {
    path: 'actors',
    component: ShowsPageComponent,
    children: [
      {
        path: ':pageNumber',
        component: ActorsPageComponent,
      },
    ],
  },
  {
    path: 'actors/:id/main',
    component: ActorMainPageComponent,
    data: { navItem: 'Main' },
  },
  {
    path: 'profile',
    component: ProfilePageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
